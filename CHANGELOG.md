# Change Log

All notable changes to the "The Purple Stuff" VS Code theme will be documented in this file.

## 0.0.4

- Fix status bar background

## 0.0.3

- Add token colors for syntax highlighting

## 0.0.2

- Remove default editor foreground color

## 0.0.1

- Initial release
