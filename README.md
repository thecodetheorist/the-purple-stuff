# The Purple Stuff
## VS Code, but in purple

A light, easy-on-the-eyes theme contrasted with different shades of purple.

This theme was generated using the official Yeoman VS Code theme generator.

For usage instructions, please see the official VS Code documentation for creating & using themes.